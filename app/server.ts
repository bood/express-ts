import express from 'express';
const Sentry = require('@sentry/node') ;

Sentry.init({ dsn:'https://e5206227b755424a861bd6e11c0f5726@sentry.io/1295872' });

import {WelcomeController} from './controllers';

const app: express.Application = express();
const port = process.env.PORT || '3000';

app.use(Sentry.Handlers.requestHandler());

app.get('/sentry_exception', function mainHandler(req, res) {
    throw new Error('Sentry Exception: ' + new Date());
});

app.get('/sentry_reject', function mainHandler(req, res) {
    Promise.reject(new Error('Unhandled rejection: ' + new Date()));
    res.send('ok');
});

// The error handler must be before any other error middleware
app.use(Sentry.Handlers.errorHandler());

app.use('/welcome', WelcomeController);

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`);
});
